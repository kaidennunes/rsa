﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using RSA.Utils;

namespace RSA
{
	public class RSA
	{
		public static readonly int NUMBER_OF_BITS_IN_PRIME = 512;
		public static readonly BigInteger E = 65537;
		public RSAKeyHolder GenerateRSAKey()
		{
			RSAKeyHolder keyHolder = new RSAKeyHolder();
			keyHolder.E = E;

			BigInteger phi = 0;
			do
			{
				keyHolder.P = PrimeUtils.GeneratePrime(NUMBER_OF_BITS_IN_PRIME);
				keyHolder.Q = PrimeUtils.GeneratePrime(NUMBER_OF_BITS_IN_PRIME);
				keyHolder.N = keyHolder.P * keyHolder.Q;
				phi = (keyHolder.P - 1) * (keyHolder.Q - 1);
			} while (!PrimeUtils.IsRelativelyPrimeTo(phi, keyHolder.E));

			keyHolder.D = MathUtils.InverseModulo(keyHolder.E, phi);
			return keyHolder;
		}

		public BigInteger EncryptData(BigInteger data, RSAKeyHolder keyHolder)
		{
			return MathUtils.ModularExponentiation(data, keyHolder.E, keyHolder.N);
		}

		public BigInteger DecryptData(BigInteger data, RSAKeyHolder keyHolder)
		{
			return MathUtils.ModularExponentiation(data, keyHolder.D, keyHolder.N);
		}
	}
}
