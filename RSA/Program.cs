﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace RSA
{
	class Program
	{
		static void Main(string[] args)
		{
			// Allow user input that is up to 4096 bytes
			byte[] inputBuffer = new byte[4096];
			Stream inputStream = Console.OpenStandardInput(inputBuffer.Length);
			Console.SetIn(new StreamReader(inputStream, Console.InputEncoding, false, inputBuffer.Length));

			RSA encrypterRSA = new RSA();
			RSAKeyHolder keyHolder = encrypterRSA.GenerateRSAKey();

			Console.Write("P: ");
			Console.WriteLine(keyHolder.P);

			Console.Write("Q: ");
			Console.WriteLine(keyHolder.Q);

			Console.Write("N: ");
			Console.WriteLine(keyHolder.N);

			Console.Write("E: ");
			Console.WriteLine(keyHolder.E);

			Console.Write("D: ");
			Console.WriteLine(keyHolder.D);

			Console.WriteLine("Please insert a value to encrypt");
			var givenPlainData = Console.ReadLine();

			var calculatedEncryptedData = encrypterRSA.EncryptData(BigInteger.Parse(givenPlainData), keyHolder);

			Console.Write("Encrypted Data: ");
			Console.WriteLine(calculatedEncryptedData);

			Console.WriteLine("Please insert a value to decrypt");
			var givenEncryptedData = Console.ReadLine();

			var calculatedDecryptedData = encrypterRSA.DecryptData(BigInteger.Parse(givenEncryptedData), keyHolder);
			Console.Write("Decrypted Data: ");
			Console.WriteLine(calculatedDecryptedData);

			Console.Read();
		}
	}
}
