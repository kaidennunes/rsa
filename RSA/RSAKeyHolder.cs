﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace RSA
{
	public class RSAKeyHolder
	{
		public BigInteger P { get; set; }
		public BigInteger Q { get; set; }
		public BigInteger N { get; set; }
		public BigInteger E { get; set; }
		public BigInteger D { get; set; }
	}
}
