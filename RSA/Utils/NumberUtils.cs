﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Globalization;

namespace RSA.Utils
{
	public class NumberUtils
	{
		public static BigInteger ConvertHexToBigInteger(string hex)
		{
			var style = NumberStyles.HexNumber | NumberStyles.AllowHexSpecifier;
			BigInteger bigInteger = new BigInteger();
			BigInteger.TryParse(string.Format("0{0}", hex), style, null, out bigInteger);
			return bigInteger;
		}
	}
}
