﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace RSA.Utils
{
	public static class MathUtils
	{
		public static BigInteger InverseModulo(BigInteger numberToCalculateInverse, BigInteger modulo)
		{
			BigInteger m0 = modulo;
			BigInteger y = 0;
			BigInteger x = 1;

			if (modulo == 1)
			{
				return 0;
			}

			while (numberToCalculateInverse > 1)
			{
				// q is quotient 
				BigInteger q = numberToCalculateInverse / modulo;

				BigInteger t = modulo;

				// m is remainder now, process 
				// same as Euclid's algo 
				modulo = numberToCalculateInverse % modulo;
				numberToCalculateInverse = t;
				t = y;

				// Update x and y 
				y = x - q * y;
				x = t;
			}

			// Make x positive 
			if (x < 0)
			{
				x += m0;
			}

			return x;
		}

		public static BigInteger GCD(BigInteger a, BigInteger b)
		{
			if (a == 0)
			{
				return b;
			}
			if (b == 0)
			{
				return a;
			}

			if (a > b)
			{
				return GCD(a % b, b);
			}
			else
			{
				return GCD(a, b % a);
			}
		}

		public static BigInteger ModularExponentiation(BigInteger number, BigInteger exponent, BigInteger modulus)
		{
			if (exponent == 0)
			{
				return 1;
			}
			BigInteger z = ModularExponentiation(number, BigInteger.Divide(exponent, 2), modulus);
			if (exponent % 2 == 0)
			{
				return (BigInteger.Pow(z, 2)) % modulus;

			}
			return (number * (BigInteger.Pow(z, 2))) % modulus;
		}
	}
}
