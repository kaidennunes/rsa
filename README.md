RSA

To run, open the solution file (.sln) and click the green debug button (or click ctr + f5).

The values for the public and private keys will be generated automatically and printed out on the screen.

A prompt will be shown asking for some value to encrypt. After inputting the value and entering a new line, the encrypted value will appear on the screen.

A new prompt will be shown asking for some value to decrypt. After inputting the value and entering a new line, the decrypted value will appear on the screen.

After this, all printed values will remain on the screen until a key is clicked.